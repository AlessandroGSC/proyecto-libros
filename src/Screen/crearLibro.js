import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import ModalLibro from './ModalLibro';
import './Styles.css';

function CrearLibro() {
  const getData = () => {
    const titulos = JSON.parse(localStorage.getItem('titulos'));
    return titulos || [];
  };

  const defaultTitulo = { titulo: '', descripcion: '', autor: '', portada: '', leido: false };

  const [titulos, setTitulos] = useState(getData());
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const handleTareaAdded = () => {
    const titulosActualizados = getData();
    setTitulos(titulosActualizados);
    closeModal();
  };

  const openModal = () => {
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };

  const handleToggleLeido = (index) => {
    const updatedTitulos = [...titulos];
    updatedTitulos[index].leido = !updatedTitulos[index].leido;
    setTitulos(updatedTitulos);
  };

  useEffect(() => {
    setTitulos(getData());
  }, []);

  const containerStyle = {
    backgroundColor: '#F5EFE6',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  };

  return (
    <div className='container' style={containerStyle}>
      <h2
        style={{
          marginBottom: '20px',
          fontSize: '60px',
          color: '#333',
          marginTop: -20,
          fontWeight: 'bold',
        }}
      >
        App de Libros
      </h2>

      <button
        style={{
          width: 200,
          height: 45,
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#C7E7F5',
          color: '#C7E7F5',
          borderColor: '#C7E7F5',
          marginTop: 20,
        }}
        onClick={openModal}
      >
        <text style={{ color: '#000', fontSize: 18, fontWeight: 'bold' }}>
          {' '}
          Nuevo Libro
        </text>
      </button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel='Crear Libro Modal'
        style={{
          content: {
            width: '50%',
            maxHeight: '50%',
            margin: 'auto',
          },
        }}
      >
        <ModalLibro closeModal={closeModal} onTareaAdded={handleTareaAdded} />
      </Modal>

      <div style={{ overflow: 'auto', maxHeight: '350px' }}>
        {titulos.length === 0 ? (
          <p>No se encontraron resultados.</p>
        ) : (
          <table
            className='responsive-table'
            style={{
              width: '1000px',
              borderCollapse: 'collapse',
              marginTop: 10,
              border: '1px solid #ccc',
            }}
          >
            <thead>
              <tr>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  #
                </th>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  TITULO
                </th>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  Descripción
                </th>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  autor
                </th>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  Portada
                </th>
                <th
                  style={{
                    backgroundColor: '#247BA0',
                    color: '#fff',
                    padding: '10px',
                  }}
                >
                  Leído
                </th>
              </tr>
            </thead>
            <tbody>
              {titulos.map((titulo, index) => (
                <tr key={index}>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    {index + 1}
                  </td>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    {titulo.titulo}
                  </td>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    {titulo.descripcion}
                  </td>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    {titulo.autor}
                  </td>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    {titulo.portada}
                  </td>
                  <td
                    style={{
                      backgroundColor: index % 2 === 0 ? '#eee' : '#fff',
                      padding: '10px',
                    }}
                  >
                    <button
                      onClick={() => handleToggleLeido(index)}
                      style={{
                        backgroundColor: titulo.leido ? '#4CAF50' : '#C7E7F5',
                        color: '#fff',
                        border: 'none',
                        padding: '5px 10px',
                        cursor: 'pointer',
                      }}
                    >
                      {titulo.leido ? '✅' : 'Marcar leído'}
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
}

export default CrearLibro;
