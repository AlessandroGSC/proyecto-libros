import React, { useState, useEffect } from 'react';
import './Modal.css';

function ModalLibro({ closeModal, onTareaAdded }) {
  const [nombreLibro, setNombreLibro] = useState('');
  const [introduccionLibro, setIntroduccionLibro] = useState('');
  const [autorLibro, setAutorLibro] = useState('');
  const [imagenLibro, setImagenLibro] = useState(null);
  const [titulosGuardados, setTitulosGuardados] = useState([]);

  const handleCrearLibro = () => {
    saveData();
    closeModal();
    onTareaAdded();
  };

  const saveData = () => {
    const nuevosTitulos = [
      ...titulosGuardados,
      {
        titulo: nombreLibro,
        descripcion: introduccionLibro,
        autor: autorLibro,
        imagen: imagenLibro,
      },
    ];
    setTitulosGuardados(nuevosTitulos);
    localStorage.setItem('titulos', JSON.stringify(nuevosTitulos));

    alert('Se ha guardado correctamente ');
  };

  useEffect(() => {
    const titulosGuardados = JSON.parse(localStorage.getItem('titulos')) || [];
    setTitulosGuardados(titulosGuardados);
  }, []);

  return (
    <div className="modal-container">
      <h1 className="modal-title">Crear tarea</h1>
      <div className="modal-input-container">
        <label className="modal-label" htmlFor="tituloLibro">
          Título
        </label>
        <input
          type="text"
          id="tituloLibro"
          placeholder="Titulo del Libro"
          value={nombreLibro}
          className="modal-input"
          onChange={(e) => setNombreLibro(e.target.value)}
        />
      </div>
      <div className="modal-input-container">
        <label className="modal-label" htmlFor="autorLibro">
          Autor
        </label>
        <input
          type="text"
          id="autorLibro"
          placeholder="Autor del libro"
          value={autorLibro}
          onChange={(e) => setAutorLibro(e.target.value)}
          className="modal-input"
        />
      </div>
      <div className="modal-input-container">
        <label className="modal-label" htmlFor="introduccionLibro">
          Descripción
        </label>
        <input
          type="text"
          id="introduccionLibro"
          placeholder="Introduccion del libro"
          value={introduccionLibro}
          onChange={(e) => setIntroduccionLibro(e.target.value)}
          className="modal-input"
        />
      </div>
      <div className="modal-input-container">
        <label className="modal-label" htmlFor="imagenLibro">
          Imagen del libro
        </label>
        <input
          type="file"
          id="imagenLibro"
          onChange={(e) => setImagenLibro(e.target.files[0])}
          className="modal-input"
        />
      </div>
      {imagenLibro && (
        <div className="modal-input-container">
          <img
            src={URL.createObjectURL(imagenLibro)}
            alt="Vista previa de la imagen"
            style={{ maxWidth: '100%', maxHeight: '200px' }}
          />
        </div>
      )}
      <button className="modal-button" onClick={handleCrearLibro}>
        <text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>
          {' '}
          Crear Libro
        </text>
      </button>
    </div>
  );
}

export default ModalLibro;
